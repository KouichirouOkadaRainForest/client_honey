#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import os
import csv
import glob
import json
import shutil
import socket
import time
import yara
import datetime
import geoip2.database

import argparse
import logging
import traceback

#import logstash
#from logstash.formatter import LogstashFormatterVersion1

import elasticsearch
from urllib.parse import urlparse

from elasticsearch import Elasticsearch, connection
from elasticsearch_dsl import Search

import configparser

project_name = "rftb3"

gDataStoreDir = ""

config_ini = configparser.ConfigParser()
awsauth = None
es_client = None
try :
    config_ini.read('elastic.ini', encoding='utf-8')
    host = [config_ini['Elastic']['host']]
    scheme = config_ini['Elastic']['scheme']
    port = int(config_ini['Elastic']['port'])
    try :
        awsauth = ( config_ini['Elastic']['user'], config_ini['Elastic']['password'] )
    except :
        pass

    es_client = None
    if host != '' :
        if scheme == 'https' :
            es_client = Elasticsearch(
                hosts=host,
                http_auth=awsauth,
                use_ssl=True,
                verify_certs=True,
                scheme="https",
                port=port,
                timeout=60,
                connection_class=elasticsearch.connection.RequestsHttpConnection
                )
        else :
            es_client = Elasticsearch(
                hosts=host,
                http_auth=awsauth,
                scheme="http",
                port=port,
                timeout=60,
                connection_class=elasticsearch.connection.RequestsHttpConnection
                )
except Exception as e:
    pass
#    t, v, tb = sys.exc_info()
#    print(traceback.format_exception(t,v,tb))
#    print(traceback.format_tb(e.__traceback__))

getip_db = geoip2.database.Reader('geoip/GeoLite2-City.mmdb')

def foward_lookup(domain):
    try:
            return socket.gethostbyname(domain)
    except:
            return "None"

def get_geoip( ip ) :
    result = {}

    try :
        geom_info = getip_db.city( ip )

        try :
            result["continent"] = geom_info.country.iso_code
        except :
            result["continent"] = ""

        try :
            result["registered_country"] = geom_info.registered_country.names["en"]
        except :
            result["registered_country"] = ""

        try :
            result["time_zone"] = geom_info.location.time_zone
        except :
            result["time_zone"] = None
        try :
            result["coordinates"] = [geom_info.location.longitude, geom_info.location.latitude]
        except :
            result["coordinates"] = None
    except :
        pass
    return result

har_parser = None
def load_har( har_file ) :
    global har_parser

    with open(har_file, 'r') as f:
        har_parser = json.loads(f.read())

def store_info( session_id, date, seed, crawl_data ) :
    global ec_client
    global har_parser
    global project_name
    global gDataStoreDir

    entry = {}
    entry["session_id"] = session_id
#    entry["user_id"] = uid
    entry["seed"] = seed
    entry["crawl_date"] = date

    entry["title"] = crawl_data['title']
    entry["console"] = crawl_data['console']
    for item in entry["console"] :
        if (type(item["msg"]) is str) == False :
            item["msg"] = json.dumps(item["msg"])
    entry["coockies"] = crawl_data['coockies']
    entry["sandbox"] = crawl_data['sandbox']

    install_data = json.loads( json.dumps( entry ) )
    #print(install_data)
    index_name =  "%s-info_index-%s" % (project_name,date.split(" ")[0].replace("/","."))
    if es_client != None :
        es_client.index(index=index_name, doc_type="har", body=install_data)
    else :
        print("[%s]"%index_name)
        print(install_data)

    # Save to File
    save_filename = os.path.join( gDataStoreDir, "info.json")
    json.dump(install_data, open(save_filename,"w"))

def store_link( session_id, date, seed, crawl_data ) :
    global ec_client
    global har_parser
    global project_name
    global gDataStoreDir

    entry = {}
    entry["session_id"] = session_id
#    entry["user_id"] = uid
    entry["seed"] = seed
    entry["crawl_date"] = date

    entry["image"] = {"same": crawl_data['images'], "outer":crawl_data['images_o'] }
    entry["link"] = {"same": crawl_data['links'], "outer":crawl_data['links_o'] }
    entry["script"] = {"same": crawl_data['scripts'], "outer":crawl_data['scripts_o']}
    entry["script_inline"] = crawl_data['inline']
    entry["anchor"] = {"same": crawl_data['anchors'], "outer":crawl_data['anchors_o'] }
    entry["form"] = {"same": crawl_data['forms'], "outer":crawl_data['forms_o'] }

    install_data = json.loads( json.dumps( entry ) )
    #print(install_data)
    index_name =  "%s-info_link-%s" % (project_name,date.split(" ")[0].replace("/","."))
    if es_client != None :
        es_client.index(index=index_name, doc_type="har", body=install_data)
    else :
        print("[%s]"%index_name)
        print(install_data)

    # Save to File
    save_filename = os.path.join( gDataStoreDir, "link.json")
    json.dump(install_data, open(save_filename,"w"))

def store_har( session_id, date, seed ) :
    global ec_client
    global har_parser
    global project_name
    global gDataStoreDir

    access_domains = {}
    contents_type = {}

    entrys = []
    index = 1
    for entry_tmp in har_parser["log"]["entries"] :
        entry = json.loads( json.dumps( entry_tmp ) )

        entry["session_id"] = session_id
#        entry["user_id"] = uid
        entry["seed"] = seed
        entry["crawl_date"] = date
        entry["_initiator_detail"] = json.loads(entry["_initiator_detail"])

        parse_result = urlparse( entry["request"]["url"] )
        if parse_result.netloc not in access_domains :
            ip = foward_lookup(parse_result.netloc)
            if ip != "None" : 
                access_domains[parse_result.netloc] = { "domain": parse_result.netloc, "num": 0, "ip": ip }
            else :
                access_domains[parse_result.netloc] = { "domain": parse_result.netloc, "num": 0, "ip": False }
        access_domains[parse_result.netloc]["num"] += 1

        content_type = entry["response"]["content"]["mimeType"]
        if content_type not in contents_type :
            contents_type[content_type] = { "content_type": content_type, "num": 0 }
        contents_type[content_type]["num"] += 1

        del entry["request"]["headers"]
        del entry["request"]["cookies"]
        if "queryString" in entry["request"] :
            del entry["request"]["queryString"]
        if "postData" in entry["request"] :
            del entry["request"]["postData"]
        del entry["response"]["headers"]
        del entry["response"]["cookies"]
        entry["response"]["transferSize"] = entry["response"]["_transferSize"]
        del entry["response"]["_transferSize"]
        entry["timings"]["queued"] = entry["timings"]["_queued"]
        del entry["timings"]["_queued"]

        if "_initiator" in entry :
            entry["initiator"] = entry["_initiator"]
            del entry["_initiator"]
        if "_priority" in entry :
            entry["priority"] = entry["_priority"]
            del entry["_priority"]
        if "_initiator_detail" in entry :
            entry["initiator_detail"] = entry["_initiator_detail"]
            del entry["_initiator_detail"]
        if "_requestTime" in entry :
            entry["requestTime"] = entry["_requestTime"]
            del entry["_requestTime"]
        if "_initiator_type" in entry :
            entry["initiator_type"] = entry["_initiator_type"]
            del entry["_initiator_type"]
        if "_initiator_line" in entry :
            entry["initiator_line"] = entry["_initiator_line"]
            del entry["_initiator_line"]
        if "_initiator_function_name" in entry :
            entry["initiator_function_name"] = entry["_initiator_function_name"]
            del entry["_initiator_function_name"]
        if "_initiator_column" in entry :
            entry["initiator_column"] = entry["_initiator_column"]
            del entry["_initiator_column"]
        if "_initialPriority" in entry :
            entry["initialPriority"] = entry["_initialPriority"]
            del entry["_initialPriority"]
        if "_initiator_script_id" in entry :
            entry["initiator_script_id"] = entry["_initiator_script_id"]
            del entry["_initiator_script_id"]

        install_data = json.loads( json.dumps( entry ) )
        #print(install_data)
        index_name =  "%s-har_index-%s" % (project_name,date.split(" ")[0].replace("/","."))
        if es_client != None :
            es_client.index(index=index_name, doc_type="har", body=install_data)
        else :
            print("[%s]"%index_name)
            print(install_data)
            pass
        index += 1
        entrys.append( install_data )

    # Save to File
    save_filename = os.path.join( gDataStoreDir, "har.json")
    json.dump(entrys, open(save_filename,"w"))

    return entrys

def store_domain( session_id, date, seed ) :
    global ec_client
    global har_parser
    global project_name
    global gDataStoreDir

    access_domains = {}
    contents_type = {}

    entrys = []
    for entry_tmp in har_parser["log"]["entries"] :
        entry = json.loads( json.dumps( entry_tmp ) )

        entry["session_id"] = session_id
#        entry["user_id"] = uid
        entry["seed"] = seed
        entry["crawl_date"] = date

        entry["_initiator_detail"] = json.loads(entry["_initiator_detail"])

        parse_result = urlparse( entry["request"]["url"] )
        if parse_result.netloc not in access_domains :
            ip = foward_lookup(parse_result.netloc)
            if ip != "None" : 
                access_domains[parse_result.netloc] = { "domain": parse_result.netloc, "num": 0, "ip": ip }
            else :
                access_domains[parse_result.netloc] = { "domain": parse_result.netloc, "num": 0, "ip": False }
        access_domains[parse_result.netloc]["num"] += 1

        content_type = entry["response"]["content"]["mimeType"]
        if content_type not in contents_type :
            contents_type[content_type] = { "content_type": content_type, "num": 0 }
        contents_type[content_type]["num"] += 1

    # Access Domain
    for key in access_domains :
        data = access_domains[key]
        data["session_id"] = session_id
#        data["user_id"] = uid
        data["seed"] = seed
        data["crawl_date"] = date
        data["geoip"] = get_geoip( data["ip"] )

        install_data = json.loads( json.dumps( data ) )
#        print(install_data)
        index_name =  "%s-domain_index-%s" % (project_name,date.split(" ")[0].replace("/","."))
        if es_client != None :
            es_client.index(index=index_name, doc_type="har", body=install_data)
        else :
            print("[%s]"%index_name)
            print(install_data)
            pass
        entrys.append( install_data )

    # Save to File
    save_filename = os.path.join( gDataStoreDir, "domain.json")
    json.dump(entrys, open(save_filename,"w"))

    return entrys

def store_contenttype( session_id, date, seed ) :
    global ec_client
    global har_parser
    global project_name
    global gDataStoreDir

    access_domains = {}
    contents_type = {}

    entrys = []
    for entry_tmp in har_parser["log"]["entries"] :
        entry = json.loads( json.dumps( entry_tmp ) )

        entry["session_id"] = session_id
#        entry["user_id"] = uid
        entry["seed"] = seed
        entry["crawl_date"] = date

        entry["_initiator_detail"] = json.loads(entry["_initiator_detail"])

        parse_result = urlparse( entry["request"]["url"] )
        if parse_result.netloc not in access_domains :
            ip = foward_lookup(parse_result.netloc)
            if ip != "None" : 
                access_domains[parse_result.netloc] = { "domain": parse_result.netloc, "num": 0, "ip": ip }
            else :
                access_domains[parse_result.netloc] = { "domain": parse_result.netloc, "num": 0, "ip": False }
        access_domains[parse_result.netloc]["num"] += 1

        content_type = entry["response"]["content"]["mimeType"]
        if content_type not in contents_type :
            contents_type[content_type] = { "content_type": content_type, "num": 0 }
        contents_type[content_type]["num"] += 1

    # Contents Type
    for key in contents_type :
        data = contents_type[key]
        data["session_id"] = session_id
        data["seed"] = seed
#        data["user_id"] = uid
        data["crawl_date"] = date

        install_data = json.loads( json.dumps( data ) )
#        print(install_data)
        index_name =  "%s-contenttype_index-%s" % (project_name,date.split(" ")[0].replace("/","."))
        if es_client != None :
            es_client.index(index=index_name, doc_type="har", body=install_data)
        else :
            print("[%s]"%index_name)
            print(install_data)
            pass
        entrys.append( install_data )

    # Save to File
    save_filename = os.path.join( gDataStoreDir, "contenttype.json")
    json.dump(entrys, open(save_filename,"w"))

    return entrys

def hist_ContentType( session_id, date, seed, datas ) :
    global ec_client
    global har_parser
    global project_name
    global gDataStoreDir

    har_data = []
    for data in datas :
        seed_netloc = urlparse( data['seed'] ).netloc
        req_netloc = urlparse( data['request']['url'] ).netloc
        res_status = data['response']['status']
        res_type = data['response']['content']['mimeType']
        res_size = data['response']['content']['size']
        connect = data['timings']['connect']
        receive = data['timings']['receive']
        har_data.append( {
            'seed_netloc' : seed_netloc,
            'req_netloc' : req_netloc,
            'res_status' : res_status,
            'res_type' : res_type,
            'res_size' : res_size,
            'connect' : connect,
            'receive' : receive,
            } )

    ret = { }
    for data in har_data :
        key1 = data['res_type']
        if len(key1) == 0:
            continue

        if key1 not in ret :
            ret[key1] = {'count': 0}
        sub_key1 = data['req_netloc']
        if sub_key1 not in ret[key1] :
            ret[key1][sub_key1] = { 'count': 0, 'size': 0, 'time': 0, 'time_ave': 0, 'time_max': 0 }

        ret[key1]['count'] += 1

        cur = ret[key1][sub_key1]
        cur['count'] += 1
        if cur['time_max'] < data['receive'] :
            cur['time_max'] = data['receive']
        cur['size'] += data['res_size']
        cur['time'] += data['receive']
        cur['time_ave'] = ret[key1][sub_key1]['time']/ret[key1][sub_key1]['count']

        cur['time'] = round(cur['time'], 2)
        cur['time_max'] = round(cur['time_max'], 2)
        cur['time_ave'] = round(cur['time_ave'], 2)

    entrys = []
    for type_str in ret :
        send_data = { 'content_type': type_str }
        send_data["session_id"] = session_id
        send_data["seed"] = seed
        send_data["crawl_date"] = date
        send_data.update( ret[type_str] )

        index_name =  "%s-harhist_index-%s" % (project_name,date.split(" ")[0].replace("/","."))
        if es_client != None :
            es_client.index(index=index_name, doc_type="har", body=send_data)
        else :
            print("[%s]"%index_name)
            print(json.dumps(send_data,indent=2))
        entrys.append( send_data )

    # Save to File
    save_filename = os.path.join( gDataStoreDir, "hist_contenttype.json")
    json.dump(entrys, open(save_filename,"w"))

def yara_result( session_id, date, seed, data_dir ) :
    global project_name
    global gDataStoreDir

    rule_path = os.path.join(os.path.dirname( os.path.abspath( __file__ ) ), 'yara/goosec_index.yar')
    rule = yara.compile(filepath=rule_path)

    result = []
    for filename in glob.glob(os.path.join( data_dir, "*")) :
        f = open( filename )
        data = f.read()
        f.close()

        f_hash = filename.split("/")[-1]
        #cur = {"hash":f_hash, "code": data, "yara":[]}
        cur = {"hash":f_hash, "yara":[]}
        result.append( cur )

        matches = rule.match(data=data)
        if len(matches) > 0 :
            for item in matches :
                print( item.rule )
                cur["yara"].append( item.rule )

    send_data = { }
    send_data["session_id"] = session_id
    send_data["seed"] = seed
    send_data["crawl_date"] = date
    send_data["js_data"] = result

    index_name =  "%s-js_data_index-%s" % (project_name,date.split(" ")[0].replace("/","."))
    if es_client != None :
        es_client.index(index=index_name, doc_type="har", body=send_data)
    else :
        print("[%s]"%index_name)
        print(json.dumps(send_data,indent=2))

    # Save to File
    save_filename = os.path.join( gDataStoreDir, "yara.json")
    json.dump(send_data, open(save_filename,"w"))

def analyse( session_id, date, path, js_path ) :
    global har_parser
    global gDataStoreDir

    crawl_data = json.load( open(path) )

    har_parser = crawl_data['har']
    
    store_info( session_id, date, crawl_data['url'], crawl_data )
    store_link( session_id, date, crawl_data['url'], crawl_data )

    store_domain( session_id, date, crawl_data['url'])
    store_contenttype( session_id, date, crawl_data['url'])

    h_data = store_har( session_id, date, crawl_data['url'])
    hist_ContentType(session_id, date, crawl_data['url'], h_data)
    
    yara_result( session_id, date, crawl_data['url'], js_path)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='install har')
    parser.add_argument('session', help='session id')
#    parser.add_argument('id', help='user id')
    #parser.add_argument('timestamp', help='timestamp')
    args = parser.parse_args()
    session = args.session
#    uid = "none"

    #timestamp = args.timestamp
    now = datetime.datetime.now()
    timestamp = now.strftime("%Y/%m/%d %H:%M:%S")

    gDataStoreDir = os.path.join( os.environ.get('CRAWL_DATA', './tmp'), session )
    data_path = os.path.join( gDataStoreDir, "goosec.json")
    js_data_path = os.path.join( gDataStoreDir, "sandbox")

#    if data_path == "now" :
#        import datetime
#        now = datetime.datetime.now()
#        data_path = os.path.join("./datas/contents/",now.strftime("%Y%m%d%H"))

    analyse( session, timestamp, data_path, js_data_path )
    

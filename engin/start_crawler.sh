#!/bin/bash
SCRIPT_DIR=$(cd $(dirname $0); pwd)

rm -f /var/log/chrome/*
node client_honey.js $1 -u $2
python3 analyzer.py $1
killall chrome
rm -f /var/log/chrome/*

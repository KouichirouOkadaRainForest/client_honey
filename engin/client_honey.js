const fs = require('fs');
const path = require('path');
const crypt = require('crypto');

const beautify = require('js-beautify').js;

const Puppeteer = require('puppeteer');
const PuppeteerHar = require('puppeteer-har');
const extractDomain = require('extract-domain');
const ArgumentParser = require('argparse').ArgumentParser;
const victim = require('./sandbox');

const loadPlugins = require('plugin-system');

result = { 
	  url:"", 
	  title:"", 
	  images:[], 
	  images_o:[], 
	  links:[], 
	  links_o:[], 
	  scripts:[], 
	  scripts_o:[], 
	  inline:{}, 
	  anchors:[], 
	  anchors_o:[], 
	  forms:[], 
	  forms_o:[], 
	  coockies:[], 
	  har:[],
	  console:[],
	  sandbox:[]
};

result.inline["source"] = "";

contents_list = [];

// mkdir Save Directory
gDataHome = "./tmp";
gDataHome_Session = "./tmp";

//  await page.tracing.start({
async function recv_response_data(response) {
    const url = response.url();
    const header = response.headers();
    const type = header['content-type'];
    const code = parseInt(response.headers().status);
    let buf;
    try {
        buf = await response.buffer();
    } catch (e) {
        return;
    }
    if (!buf) {
        return;
    }

    // Contents HASH
    let hashsum = crypt.createHash('md5');
    hashsum.update(buf);
    const hash = hashsum.digest('hex');
    filename = hash+".dat";
    contents_list.push( {"type": type, "name": filename} )

    dst_file = path.join(gDataHome_Session, "contents.lst");
    fs.writeFileSync(dst_file, JSON.stringify(contents_list, null, 4));

    // Contents SaveDir
    dst_dir = path.join(gDataHome_Session, "contents");
    if (!fs.existsSync( dst_dir )) { fs.mkdirSync( dst_dir ); }
    dst_file = path.join(dst_dir, filename);
    fs.writeFileSync(dst_file, buf);

    // Sandbox Analysis
    if ((typeof type == 'string') && (type.indexOf('javascript') != -1)) {
        try {
            var sandbox = new victim.Sandbox();
            sandbox.run(buf);
            result['sandbox'].push( {url: url, hash: hash, log: sandbox.get_jslog()} );
        } catch (e) {
            return;
        }
    }
}

async function crawl_url( session, browser, url ) {
  // mkdir Save Directory
  if(process.env.CRAWL_DATA) { gDataHome = process.env.CRAWL_DATA; }
  if (!fs.existsSync( gDataHome )) { fs.mkdirSync( gDataHome ); }
  gDataHome_Session = path.join(gDataHome, session);
  if (!fs.existsSync( gDataHome_Session )) { fs.mkdirSync( gDataHome_Session ); }

  const page = await browser.newPage();

//    path: 'trace.json',
//    categories: ['devtools.timeline']
//  })

  page.on("console", msg => {
    for (let arg of msg.args()) {
      arg.jsonValue().then(v => {
        result.console.push({caller:msg._location, msg:v});
      });
    }
  });
  page.on('response', (response) => recv_response_data(response));
//  await page.emulate(devices['iPhone 6'])

  const har = new PuppeteerHar(page);
  await har.start();
  page.on('dialog', async dialog => {
    console.log(dialog.message())
    await dialog.dismiss()
  })
  await page.goto( url, {
	  //referer: "https://www.google.com/"
	  referer: "http://www.bing.yahoo.ask.duckduckgo.qwant.google.com"
  });

  var base_hostname = extractDomain(url);

  result.url = url
  //result.title = await page.title()

  console.log("###### GET INFO #####");
  try {
    const images = await page.$$eval('img', anchors => { return anchors.map(anchor => anchor.src) })
    images.forEach(function( value ) {
      var tmp = extractDomain( value );
      if ( base_hostname != tmp ) {
        if (result.images_o.indexOf( value ) == -1) {
          result.images_o.push( value );
        }
      } else {
        if (result.images.indexOf( value ) == -1) {
          result.images.push( value );
        }
      }
    });
  } catch (e) {
  }

  try {
    const links = await page.$$eval('link', anchors => { return anchors.map(anchor => anchor.href) })
    links.forEach(function( value ) {
      var tmp = extractDomain( value );
      if ( base_hostname != tmp ) {
        if (result.links_o.indexOf( value ) == -1) {
          result.links_o.push( value );
        }
      } else {
        if (result.links.indexOf( value ) == -1) {
          result.links.push( value );
        }
      }
    });
  } catch (e) {
  }
 
  try {
    const scripts = await page.$$eval('script', anchors => { return anchors.map(anchor => anchor.src) })
    scripts.forEach(function( value ) {
      var tmp = extractDomain( value );
      if ( base_hostname != tmp ) {
        if (result.scripts_o.indexOf( value ) == -1) {
          result.scripts_o.push( value );
        }
      } else {
        if (result.scripts.indexOf( value ) == -1) {
          result.scripts.push( value );
        }
      }
    });
  } catch (e) {
  }

  try {
    const scripts_source = await page.$$eval('script', anchors => { return anchors.map(anchor => anchor.innerText) })
    scripts_source.forEach(function( value ) {
      value = value.trim();
      if(value.length > 0) {
//      let hashsum = crypt.createHash('md5');
//      hashsum.update(value);
//      const hash = hashsum.digest('hex');
        result.inline["source"] += value;
//      result.inline[hash] = value;
      }
    });
  } catch (e) {
  }

  try {
    const hrefs = await page.$$eval('a', anchors => { return anchors.map(anchor => anchor.href) })
    hrefs.forEach(function( value ) {
      var tmp = extractDomain( value );
      if ( base_hostname != tmp ) {
        if (result.anchors_o.indexOf( value ) == -1) {
          result.anchors_o.push( value );
        }
      } else {
        if (result.anchors.indexOf( value ) == -1) {
          result.anchors.push( value );
        }
      }
    });
  } catch (e) {
  }

  try {
    const actions = await page.$$eval('form', anchors => { return anchors.map(anchor => anchor.action) })
    actions.forEach(function( value ) {
      var tmp = extractDomain( value );
      if ( base_hostname != tmp ) {
        if (result.forms_o.indexOf( value ) == -1) {
          result.forms_o.push( value );
        }
      } else {
        if (result.forms.indexOf( value ) == -1) {
          result.forms.push( value );
        }
      }
  });
  } catch (e) {
  }

  const cookies = await page.cookies()
  cookies.forEach(function( value ) {
    if (result.coockies.indexOf( value ) == -1) {
        result.coockies.push( value );
    }
  });

  // Save Snapshot
  console.log( ">>>> SAVE SNAPSHOT" );
  dst_file = path.join(gDataHome_Session, "snapshot.png");
  //await page.screenshot({path: dst_file, fullPage:true})
  console.log( dst_file );
  await page.screenshot({path: dst_file, fullPage: true, type: "png"})

  result.har = await har.stop();

  result.inline["source"] = beautify(result.inline["source"], { indent_size: 2, space_in_empty_paren: true });

  // Plug-in
  console.log( ">>>> EXEC PLUGIN" );
  loadPlugins({
    paths: [
      path.join(__dirname, './plugins/'),
    ],
    }).then(function onSuccess(plugins) {
        for(let func of plugins) {
            func(gDataHome_Session);
        }
    }).catch(function onError(err) {
            console.log( "---------" );
            console.log( err );
    });

  // Save JSON Data
  dst_file = path.join(gDataHome_Session, "goosec.json");
  console.log( "--[DataSave]--:"+dst_file );
  fs.writeFileSync(dst_file, JSON.stringify(result, null, 4));

  return (result);
}

if(require.main === module) {
  var parser = new ArgumentParser({
    version: '1.0.0',
    addHelp:true,
    description: 'puppeteer crawler'
  });

  parser.addArgument( ['session'], { help: 'session id' } );
  parser.addArgument( ['-u','--url'], { help: 'url' } );

  var args = parser.parseArgs();
  var url = args.url;
  var session = args.session;

  (async () => {
    //const browser = await Puppeteer.launch( {headless: false} );
    conf = {}
    conf["headless"] = true
    //conf["headless"] = false
    conf["args"] = [ '--no-sandbox', '--disable-setuid-sandbox', '--unhandled-rejections=strict' ]
    if(process.env.CHROME_PATH != undefined) {
      conf["executablePath"] = process.env.CHROME_PATH;
    }
    console.log( conf )
    const browser = await Puppeteer.launch( conf );
  
    info = await crawl_url( session, browser, url );

    await browser.close();

    console.log("###### DONE #####");
  })();
}


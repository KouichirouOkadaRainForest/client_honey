import glob
import yara
import json

rule = yara.compile(filepath='./yara/goosec_index.yar')

result = []
for filename in glob.glob("/home/develop/cr_data/1130916b-6518-4991-a7bb-eb92cf4257fa/sandbox/*") :
    f = open( filename )
    data = f.read()
    f.close()

    f_hash = filename.split("/")[-1]
    #cur = {"hash":f_hash, "code": data, "yara":[]}
    cur = {"hash":f_hash, "yara":[]}
    result.append( cur )

    matches = rule.match(data=data)
    if len(matches) > 0 :
        for item in matches :
            print( item.rule )
            cur["yara"].append( item.rule )

print( json.dumps(result, indent=2) )

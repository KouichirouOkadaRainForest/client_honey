'use strict';

const vm = require('vm');
//const jsdom = require("jsdom");
//const { JSDOM } = jsdom;
const Window = require('window');

var js_log = [];

const __loggers__ = {};
function log(tag) {
    if (__loggers__.hasOwnProperty(tag))
      return __loggers__[tag];

    return (__loggers__[tag] = function() {
      let args = [].slice.call(arguments).map(e => typeof e === 'string' ? `"${e}"` : e);
      //console.log(`>> ${tag}(${args})\n`);
      js_log.push( {tag: tag, args: args} );
    });
}

//module.exports = class Sandbox {
class Sandbox {
  constructor(opt) {
    //const { window } = new JSDOM("<!DOCTYPE html><p>Hello world</p>");
    const window = new Window();
    this.opt = opt || {};
    let sandbox = this.sandbox = {
      __Function__: code => (() => sandbox.eval(code)),
      document: {
        write: log('document.write'),
        writeln: log('document.write')
      }, 
      window: window,
      alert: log('alert')
    };
    let self = this;
    // hook eval
    ['setTimeout', 'setInterval', 'eval'].forEach(func => sandbox[func] = (() => {
      return code => {
        log(func)(code);
        return self.run(code);
      }
    })());

    let context = self.context = vm.createContext(sandbox);
    let monkeyPatch = new vm.Script('\'use strict\';Function.prototype.constructor = __Function__;');
    monkeyPatch.runInContext(context);
  }

  get_jslog() {
    return (js_log);
  }

  run(code) {
    js_log = [];

    code = (code || '') + '';
    
    let jjencodePrefix = 'return';
    if (code.indexOf(jjencodePrefix) === 0) {
      code = `(function () {${code}})()`;
    }

//    const { window } = new JSDOM(``);
//    const { window } = new JSDOM(``, { runScripts: "outside-only" });
//    var window = {};
//    window.window = window;
//    this.sandbox.__proto__ = window;
//    this.sandbox.window = this.sandbox;

    try {
      vm.createContext(this.sandbox);
      //return vm.runInContext(code, {timeout: this.opt.timeout || 100});
      return vm.runInContext(code, this.sandbox, {timeout: this.opt.timeout || 100});
    } catch(err) {
      //console.error(`[*] "${err.message}" occured while executing "${code}"`);
      //console.log(`[*] "${err.message}" occured while executing "${code}"`);
      js_log.push( {tag: "error", args: err.message} );
    }
  }
}

module.exports.Sandbox = Sandbox;

if(require.main === module) {
  var sandbox = new Sandbox();
  sandbox.run("eval('var a=1;');");
  console.log(JSON.stringify(sandbox.get_jslog(), null, 4));
}


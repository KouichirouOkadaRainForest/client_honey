const fs = require('fs');
const path = require('path');
const glob = require("glob");
const md5 = require('md5');
const beautify = require('js-beautify').js;

module.exports = function doCopyJS( save_dir ) {
  var target_dir = "/var/log/chrome/";
  var save_dir = path.join(save_dir, "sandbox");

  if (!fs.existsSync(save_dir)) {
    fs.mkdirSync(save_dir);
  }

  glob(path.join(target_dir, "*.log"), function (er, files) {
    for(let filepath of files) {
      fs.readFile(filepath, 'utf8', function (err, data) {
        if (err) {
          throw err;
        }
        code = beautify(data, { indent_size: 2, space_in_empty_paren: true });
        hash = md5( code )

        var save_file = path.join(save_dir, hash);
	fs.writeFileSync(save_file, code);

	fs.unlinkSync(filepath);
      });
    }
  });
  
//  console.log( save_dir );
  return 'Test';
};

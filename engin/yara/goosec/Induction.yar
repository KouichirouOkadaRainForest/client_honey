/*
    This Yara ruleset is under the GNU-GPLv2 license (http://www.gnu.org/licenses/gpl-2.0.html) and open to any user or organization, as    long as you use it under this license.

*/

rule CheckRegerrer
{
   meta:
      description = "check referrer"
      ref = ""
      author = "RainForest"
      date = ""
      version = "1"
      impact = 3
      hide = false
   strings:
      $item1 = "document"
      $item2 = "referrer"
      $item3 = "indexOf"
   condition:
      $item1 and $item2 and $item3
}

rule LocationRaplace
{
   meta:
      description = "location replace"
      ref = ""
      author = "RainForest"
      date = ""
      version = "1"
      impact = 3
      hide = false
   strings:
      $item1 = "location.replace"
   condition:
      $item1
}


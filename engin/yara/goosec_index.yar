/*
GOOSEC
*/
include "./exploit_kits/EK_Angler.yar"
include "./exploit_kits/EK_Blackhole.yar"
include "./exploit_kits/EK_BleedingLife.yar"
include "./exploit_kits/EK_Crimepack.yar"
include "./exploit_kits/EK_Eleonore.yar"
include "./exploit_kits/EK_Fragus.yar"
include "./exploit_kits/EK_Phoenix.yar"
include "./exploit_kits/EK_Sakura.yar"
include "./exploit_kits/EK_ZeroAcces.yar"
include "./exploit_kits/EK_Zerox88.yar"
include "./exploit_kits/EK_Zeus.yar"

include "./webshells/WShell_APT_Laudanum.yar"
include "./webshells/WShell_ASPXSpy.yar"
include "./webshells/WShell_Drupalgeddon2_icos.yar"
include "./webshells/WShell_PHP_Anuna.yar"
include "./webshells/WShell_PHP_in_images.yar"
include "./webshells/WShell_THOR_Webshells.yar"
include "./webshells/Wshell_ChineseSpam.yar"
include "./webshells/Wshell_fire2013.yar"

include "./packers/JJencode.yar"
include "./packers/Javascript_exploit_and_obfuscation.yar"

include "./cve_rules/CVE-2010-0805.yar"
include "./cve_rules/CVE-2010-0887.yar"
include "./cve_rules/CVE-2010-1297.yar"
include "./cve_rules/CVE-2012-0158.yar"
include "./cve_rules/CVE-2013-0074.yar"
include "./cve_rules/CVE-2013-0422.yar"
include "./cve_rules/CVE-2015-1701.yar"
include "./cve_rules/CVE-2015-2426.yar"
include "./cve_rules/CVE-2015-2545.yar"
include "./cve_rules/CVE-2015-5119.yar"
include "./cve_rules/CVE-2016-5195.yar"
include "./cve_rules/CVE-2017-11882.yar"
include "./cve_rules/CVE-2018-20250.yar"
include "./cve_rules/CVE-2018-4878.yar"

/*
Custom Rules
*/
include "./goosec/Induction.yar"


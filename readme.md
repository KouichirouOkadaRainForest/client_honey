



![https://bitbucket.org/KouichirouOkadaRainForest/client_honey/raw/db39ab5ff15ae1b1c5b480a66911116087b10dba/img/goosec_logo.png](https://bitbucket.org/KouichirouOkadaRainForest/client_honey/raw/db39ab5ff15ae1b1c5b480a66911116087b10dba/img/goosec_logo.png)

# Client Honey

### 環境変数

export CRAWL_DATA="データ保存先ディレクトリ"
export CRAWL_ENGIN="解析エンジンディレクトリ"：{client_honey展開先}/engin

export CHROME_PATH="利用するクロームブラウザのフルパス"：クローム改良実行モジュールなどを指定

### CLI(クローラ)

設定ファイル

ElasticSearchを利用する場合は下記の内容を記述したelastic.iniを作成する

​	[Elastic]
​	host=host_name or ip
​	user=name
​	password=qBbOXXXXXX
​	scheme=https
​	port=9243

インストール

​	> cd  {client_honey展開先}/engin
​	> npm install

実行

​	$ start_crawler.sh {セッション名} {クロールURL}
​		データ保存先ディレクトリの下にセッション名のディレクトリが作成されデータが保存されます。

### Webの起動

ローカルDBの初期化

​	>./clean_db.sh

アクセスユーザの追加

​	下記のようなpythonを rftb3/create_user.py として作成する

​	from django.contrib.auth import get_user_modelUserModel = get_user_model()

​	if not UserModel.objects.filter(username='ユーザ名').exists():
​    	user=UserModel.objects.create_user('ユーザ名', password='パスワード')
​    	user.is_superuser=True
​    	user.is_staff=True
​    	user.save()

​	>  ./create_user.sh

WebServerの実行

​	> ./start.sh

​	http://0.0.0.0:8888/で起動される
​	http://xxxxxxxxxx:8888/console/にブラウザでアクセスする

### WebAPI

| URLの登録                  | Method | URL                | 引数              |
| -------------------------- | ------ | ------------------ | ----------------- |
| URLの登録                  | POST   | /CrawlUrl          | url=対象URL       |
| クロール履歴               | GET    | /CrawlHistory      |                   |
| クロール情報の取得         | GET    | /sInfoIndex        | session_id=処理ID |
| リンク情報の取得           | GET    | /sInfoLink         | session_id=処理ID |
| HARデータの取得            | GET    | /sHarIndex         | session_id=処理ID |
| アクセスドメインの取得     | GET    | /sDomainIndex      | session_id=処理ID |
| 国情報の取得               | GET    | /sCountryIndex     | session_id=処理ID |
| コンテンツタイプ情報の取得 | GET    | /sContentTypeIndex | session_id=処理ID |
| HAR統計情報の取得          | GET    | /sHarHistIndex     | session_id=処理ID |
| HARサマリー情報の取得      | GET    | /sHarSummary       | session_id=処理ID |
| HAR付属情報の取得          | GET    | /sHarAttention     | session_id=処理ID |
| Sandbox解析結果の取得      | GET    | /sSandboxInfo      | session_id=処理ID |
| スナップショットの取得     | GET    | /getSnapshot       | session_id=処理ID |



from django.db import models

# Create your models here.
class CrawlHistory(models.Model):
    session_id = models.CharField(max_length=64, primary_key=True)
    url = models.CharField(max_length=2048)
    status = models.CharField(max_length=16)
    regist_date = models.DateTimeField('date published', null=True, blank=True)
    update_date = models.DateTimeField('date update',    null=True, blank=True)


# coding: utf-8

import os
import uuid
import json
import queue
import datetime
import subprocess

import threading

from django.shortcuts import render

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication, permissions

from django.core import serializers
from django.contrib.auth.models import User
from django.urls import reverse
from django.shortcuts import redirect
from django.http import JsonResponse, HttpResponse
from django.core.files import File

from webui.models import CrawlHistory
from webui.elastic import ClientHoneyData

from urllib.parse import urlencode
from tld import get_tld, get_fld

gCrawlQueue = queue.Queue()
gCrawlThread = None

#
# GLOBAL Function
#
def exec_honey(session_id, url, in_now, db_obj):
    # Update DB Status
    now = datetime.datetime.now()
    db_obj.status = 'process'
    db_obj.update_date = now
    db_obj.save()

    now = datetime.datetime.now()
    c_pwd = os.getcwd()
    e_dir = os.environ.get('CRAWL_ENGIN', '../../engin/')

    os.chdir( e_dir )
    # ./start_crawler.sh "xxx" "session-2" http://172.30.2.201/attack/ "2020/03/11 17:30:00"
    #cmd = '%s "%s" "%s" %s "%s"' % (os.path.join(e_dir, "start_crawler.sh"), e_dir, session_id, url, now.strftime("%Y/%m/%d %H:%M:%S"))
    cmd = '%s "%s" %s' % (os.path.join(e_dir, "start_crawler.sh"), session_id, url)
    print(cmd)
    #os.system( cmd )
    returncode = subprocess.Popen(cmd, shell=True)

    os.chdir( c_pwd )

    # Update DB Status
    now = datetime.datetime.now()
    db_obj.status = 'done'
    db_obj.update_date = now
    db_obj.save()

def crawl_task_thread() :
    global gCrawlQueue

    print(">>>>> Start Crawler Thread")
    while True :
        _info = gCrawlQueue.get()

        exec_honey(_info["session_id"], _info["url"], _info["regist"], _info["db"])

        gCrawlQueue.task_done()

def store_crawl_task(session_id, url, now, db_boj):
    global gCrawlQueue

    gCrawlQueue.put({"session_id":session_id, "url":url, "regist": now, "db": db_boj})
#
# 
#

#
# Web API
#
class TestAPI(APIView):
#    authentication_classes = (authentication.TokenAuthentication,)
#    permission_classes = (permissions.IsAdminUser,)
    def get(self, request, format=None):
        return Response({'get_req': True})

    def post(self, request):
        return Response({'pot_req': True})

class apiCrawlUrl(APIView):
    def post(self, request, format=None):
        global gCrawlQueue
        global gCrawlThread

        if gCrawlThread == None :
            #
            # Create Crawler Thread
            #
            print(">>>>> Crawler Thread : Start")
            gCrawlThread = threading.Thread(target=crawl_task_thread, args=())
            gCrawlThread.start()
            print(">>>>> Crawler Thread : Done")

        session_id = str(uuid.uuid4())
        url = request.POST.get("url")
        now = datetime.datetime.now()
        if url == None :
            return Response(request.POST)
        
        # Store DB
        db_obj = CrawlHistory(session_id = session_id, url = url, regist_date = now, update_date = now, status='wait')
        db_obj.save()

        #redirect_url = reverse('/console/crawl_regist_done')
        redirect_url = '/console/crawl_regist_done/'

        parameters = urlencode({"session_id": session_id, "url": url})

        #exec_honey(session_id, url, now, db_obj)
        store_crawl_task(session_id, url, now, db_obj)

        url = f'{redirect_url}?{parameters}'
        return redirect(url)

class apiCrawlHistory(APIView):
    def get(self, request, format=None):
        tmp = json.loads(serializers.serialize("json", CrawlHistory.objects.all().order_by('regist_date')))

        ret = {}
        ret["local"] = []
        ret["local"].append( {"session_id":"", "url":"", "status":"", "regist_date":"", "update": ""})
        for item in tmp :
            disp_data = []
            disp_data.append( item["pk"] )
            disp_data.append( item["fields"]["url"] )
            disp_data.append( item["fields"]["status"] )
            disp_data.append( item["fields"]["regist_date"] )
            disp_data.append( item["fields"]["update_date"] )
            ret["local"].append( disp_data )

        return Response(ret)

class searchInfoIndex(APIView):
    def get(self, request, format=None):
        obj = ClientHoneyData()
        session=request.GET.get("session_id")
        ret = obj.searchInfoIndex(session)

        return Response(ret[0])

class searchInfoLink(APIView):
    def get(self, request, format=None):
        obj = ClientHoneyData()
        session=request.GET.get("session_id")
        ret = obj.searchInfoLink(session)

        return Response(ret[0])

class searchHarIndex(APIView):
    def get(self, request, format=None):
        obj = ClientHoneyData()
        session=request.GET.get("session_id")
        ret = obj.searchHarIndex(session)

        return Response(ret)

class searchDomainIndex(APIView):
    def get(self, request, format=None):
        obj = ClientHoneyData()
        session=request.GET.get("session_id")
        tmp_ret = obj.searchDomainIndex(session)
        ret = {}
        for item in tmp_ret :
            tld = get_fld(item['domain'], fix_protocol=True)
            if tld not in ret :
                ret[tld] = {}
            if item['domain'] not in ret[tld] :
                ret[tld][item['domain']] = []
            ret[tld][item['domain']].append( item )

        return Response(ret)

class searchCountryIndex(APIView):
    def get(self, request, format=None):
        obj = ClientHoneyData()
        session=request.GET.get("session_id")
        tmp_ret = obj.searchDomainIndex(session)
        ret = {}
        for item in tmp_ret :
            cn = item["geoip"]["continent"]
            tld = get_fld(item['domain'], fix_protocol=True)
            if cn not in ret :
                ret[cn] = {}
            if tld not in ret[cn] :
                ret[cn][tld] = {}
            if item['domain'] not in ret[cn][tld] :
                ret[cn][tld][item['domain']] = []
            ret[cn][tld][item['domain']].append( item )

        return Response(ret)

class searchContentTypeIndex(APIView):
    def get(self, request, format=None):
        obj = ClientHoneyData()
        session=request.GET.get("session_id")
        tmp_ret = obj.searchContentTypeIndex(session)
        ret = {}
        for item in tmp_ret :
            ret[item['content_type']] = item

        return Response(ret)

class searchHarHistIndex(APIView):
    def get(self, request, format=None):
        obj = ClientHoneyData()
        session=request.GET.get("session_id")
        tmp_ret = obj.searchHarHistIndex(session)
        ret = {}
        for item in tmp_ret :
            ret[item['content_type']] = item

        return Response(ret)

class searchHarSummary(APIView):
    def get(self, request, format=None):
        obj = ClientHoneyData()
        session=request.GET.get("session_id")
        tmp_ret = obj.searchHarIndex(session)

        ret = []
        for item in tmp_ret:
            info = {}
            info["startedDateTime"] = item["startedDateTime"]
            info["time"] = item["time"]
            info["timings"] = item["timings"]
            info["type"] = item["response"]["content"]["mimeType"]
            info["url"] = item["request"]["url"]
            info["method"] = item["request"]["method"]
            info["initiator_type"] = item["initiator_type"]
            initiator = {}
            try :
                initiator["call"] = item["initiator"]
                initiator["line"] = item["initiator_line"]
                try :
                    initiator["function_name"] = item["function_name"]
                except :
                    initiator["function_name"] = "None"
            except :
                pass
            info["initiator"] = initiator

            ret.append( info )

        return Response(ret)


class searchHarAttention(APIView):
    def get(self, request, format=None):
        obj = ClientHoneyData()
        session=request.GET.get("session_id")
        tmp_ret = obj.searchHarIndex(session)

        ret = {}
        for item in tmp_ret:
            if "css" in item["response"]["content"]["mimeType"] :
                continue
            if "image" in item["response"]["content"]["mimeType"] :
                continue
            if "other" in item["initiator_type"] :
                continue

            if item["initiator_type"] not in ret :
                ret[ item["initiator_type"] ] = []
            cur = ret[ item["initiator_type"] ]

            cur.append( {
                "from": item["initiator"], 
                "line": item["initiator_line"], 
                "type": item["response"]["content"]["mimeType"],
                "to": item["request"]["url"],
                } )

        return Response(ret)


class searchSandboxInfo(APIView):
    def get(self, request, format=None):
        obj = ClientHoneyData()
        session=request.GET.get("session_id")
        ret = obj.searchJsDataIndex(session)

        return Response(ret[0]["js_data"])

class getSnapshot(APIView):
    def get(self, request, format=None):
        session=request.GET.get("session_id")
        session=session.replace('"','')
        snapshot_path = os.path.join( os.environ.get('CRAWL_DATA', './tmp'), session, "snapshot.png")

        return HttpResponse(File(open(snapshot_path, 'rb')), content_type="image/png")

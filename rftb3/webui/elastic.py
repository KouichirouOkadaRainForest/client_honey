#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json

import datetime
from datetime import datetime, timedelta

import elasticsearch
from elasticsearch import Elasticsearch, connection
from elasticsearch_dsl import Search

import configparser

class ClientHoneyData :
    def __init__(self) :
        self.project_name = "rftb3"

        config_ini = configparser.ConfigParser()
        config_ini.read('elastic.ini', encoding='utf-8')
        host = [config_ini['Elastic']['host']]
        scheme = config_ini['Elastic']['scheme']
        port = int(config_ini['Elastic']['port'])
        try :
            awsauth = ( config_ini['Elastic']['user'], config_ini['Elastic']['password'] )
        except :
            pass

        if scheme == 'https' :
            self.client = Elasticsearch(
                hosts=host,
                http_auth=awsauth,
                use_ssl=True,
                verify_certs=True,
                scheme="https",
                port=port,
                timeout=60,
                connection_class=elasticsearch.connection.RequestsHttpConnection
                )
        else :
            self.client = Elasticsearch(
                hosts=host,
                http_auth=awsauth,
                scheme="http",
                port=port,
                timeout=60,
                connection_class=elasticsearch.connection.RequestsHttpConnection
                )

    def searchTimestamp(self, index, start, end) :
        es = Search(using=self.client, index=index) \
             .query('range', crawl_date={'gte': start , 'lt': end})

        response = es.execute()

        ret = []
        for hit in response:
            ret.append( hit.to_dict() )
        return ret

    def searchSession(self, index, session) :
        es = Search(using=self.client, index=index) \
             .query('match', session_id=session) \
             .extra(from_=0, size=10000)

        response = es.execute()

        ret = []
        for hit in response:
            ret.append( hit.to_dict() )
        return ret

    def searchInfoIndex(self, session) :
        index_name =  "%s-info_index-*" % (self.project_name)
        response = self.searchSession(index_name, session)
        return response;

    def searchInfoLink(self, session) :
        index_name =  "%s-info_link-*" % (self.project_name)
        response = self.searchSession(index_name, session)
        return response;

    def searchHarIndex(self, session) :
        index_name =  "%s-har_index-*" % (self.project_name)
        response = self.searchSession(index_name, session)
        return response;

    def searchDomainIndex(self, session) :
        index_name =  "%s-domain_index-*" % (self.project_name)
        response = self.searchSession(index_name, session)
        return response;

    def searchContentTypeIndex(self, session) :
        index_name =  "%s-contenttype_index-*" % (self.project_name)
        response = self.searchSession(index_name, session)
        return response;

    def searchHarHistIndex(self, session) :
        index_name =  "%s-harhist_index-*" % (self.project_name)
        response = self.searchSession(index_name, session)
        return response;

    def searchJsDataIndex(self, session) :
        index_name =  "%s-js_data_index-*" % (self.project_name)
        response = self.searchSession(index_name, session)
        return response;

def test( ) :
    obj = ClientHoneyData()

    session="session-1"

    ret = obj.searchInfoIndex(session)
    f = open("dev_data/searchInfoIndex.log","w")
    json.dump(ret, f)
    f.close()

    ret = obj.searchInfoLink(session)
    f = open("dev_data/searchInfoLink.log","w")
    json.dump(ret, f)
    f.close()

    ret = obj.searchHarIndex(session)
    f = open("dev_data/searchHarIndex.log","w")
    json.dump(ret, f)
    f.close()

    ret = obj.searchDomainIndex(session)
    f = open("dev_data/searchDomainIndex.log","w")
    json.dump(ret, f)
    f.close()

    ret = obj.searchContentTypeIndex(session)
    f = open("dev_data/searchContentTypeIndex.log","w")
    json.dump(ret, f)
    f.close()

    ret = obj.searchHarHistIndex(session)
    f = open("dev_data/searchHarHistIndex.log","w")
    json.dump(ret, f)
    f.close()

if __name__ == '__main__':
    test( )



# Generated by Django 3.0.4 on 2020-04-08 09:19

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='CrawlHistory',
            fields=[
                ('session_id', models.CharField(max_length=64, primary_key=True, serialize=False)),
                ('url', models.CharField(max_length=2048)),
                ('status', models.CharField(max_length=16)),
                ('regist_date', models.DateTimeField(blank=True, null=True, verbose_name='date published')),
                ('update_date', models.DateTimeField(blank=True, null=True, verbose_name='date update')),
            ],
        ),
    ]

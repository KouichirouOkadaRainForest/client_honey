# coding: utf-8

from django.conf.urls import url, include
from django.contrib import admin

from .views import TestAPI

from .views import apiCrawlUrl
from .views import apiCrawlHistory
from .views import searchInfoIndex
from .views import searchInfoLink
from .views import searchHarIndex
from .views import searchDomainIndex
from .views import searchCountryIndex
from .views import searchContentTypeIndex
from .views import searchHarHistIndex
from .views import searchHarSummary
from .views import searchHarAttention
from .views import searchSandboxInfo
from .views import getSnapshot

urlpatterns = [
    url(r'^test/', TestAPI.as_view()),

    url(r'^CrawlUrl',          apiCrawlUrl.as_view()),
    url(r'^CrawlHistory',      apiCrawlHistory.as_view()),

    url(r'^sInfoIndex',        searchInfoIndex.as_view()),
    url(r'^sInfoLink',         searchInfoLink.as_view()),
    url(r'^sHarIndex',         searchHarIndex.as_view()),
    url(r'^sDomainIndex',      searchDomainIndex.as_view()),
    url(r'^sCountryIndex',     searchCountryIndex.as_view()),
    url(r'^sContentTypeIndex', searchContentTypeIndex.as_view()),
    url(r'^sHarHistIndex',     searchHarHistIndex.as_view()),
    url(r'^sHarSummary',       searchHarSummary.as_view()),
    url(r'^sHarAttention',     searchHarAttention.as_view()),
    url(r'^sSandboxInfo',      searchSandboxInfo.as_view()),
    url(r'^getSnapshot',       getSnapshot.as_view()),
]

    function create_tabs(title, disp) {
	var class_str = "";
	if( disp ) {
	    class_str = " show active";
	}
        // TAB Header
        var tab_top = $('<li>').attr({
            class: "nav-item"
        });
        //var tab = $('<a>').attr({ class:"nav-link active", "data-toggle": "tab", role:"tab", "aria-selected": "true", href:"#"+title, "aria-controls":title });
        var tab = $('<a>').attr({
            class: "nav-link"+class_str,
            "data-toggle": "tab",
            role: "tab",
            "aria-selected": "true",
            href: "#" + title+"_tab",
            "aria-controls": title
        });
        tab.html(title)

        tab.appendTo(tab_top);
        tab_top.appendTo(".nav-tabs");

        //var top_div = $('<div>').attr({ id: title+"_tab", "aria-labelledby": title, class:"tab-pane fade show active",  role:"tabpanel" });
        var top_div = $('<div>').attr({
            id: title + "_tab",
            "aria-labelledby": title,
            class: "tab-pane fade"+class_str,
            role: "tabpanel"
        });
        var elm_1 = $('<div>').attr({
            class: "card mb-3"
        });
        var elm_1_1 = $('<div>').attr({
            class: "card-header"
        });
        var elm_1_1_1 = $('<i>').attr({
            class: "fas fa-table"
        });
        var elm_2_1 = $('<div>').attr({
            class: "card-body"
        });
        var elm_2_1_1 = $('<div>').attr({
            class: "table-responsive"
        });
        var table = $('<table>').attr({
            class: "table table-bordered",
            width: "100%",
            cellspacing: "0",
            id: title+"_table"
        });

        elm_1_1.html(title);

        table.appendTo(elm_2_1_1);
        elm_2_1_1.appendTo(elm_2_1);

        elm_1_1_1.appendTo(elm_1_1);
        elm_1_1.appendTo(elm_1);
        elm_2_1.appendTo(elm_1);

        elm_1.appendTo(top_div);
        top_div.appendTo('.tab-content');
    }

    function create_data(title, data, disp) {
        var data_top = data[0];

        var thead = $('<thead>');
        var thead_tr = $('<tr>');
        var tfoot = $('<tfoot>');
        var tfoot_tr = $('<tr>');
        for (var key in data_top) {
            var th1 = $('<th>');
            th1.html(key);
            th1.appendTo(thead_tr);
            var th2 = $('<th>');
            th2.html(key);
            th2.appendTo(tfoot_tr);
        }
        thead_tr.appendTo(thead);
        tfoot_tr.appendTo(tfoot);
        thead.appendTo("#" + title+"_table");
        tfoot.appendTo("#" + title+"_table");

        var tbody = $('<tbody>');
        for (var index in data) {
            var tbody_tr = $('<tr>');
            var item = data[index];
            for (var key in item) {
                var td = $('<td>');
		console.log(key)
		if( key == 0 ) {
		    var a = $('<a>');
		    a.attr('href' , '/console/result/?session_id="'+item[key]+'"');
		    a.attr('target' , '_blank');
                    a.html(item[key]);
                    a.appendTo(td);
		} else {
                    td.html(item[key]);
		}
                td.appendTo(tbody_tr);
            }

            tbody_tr.appendTo(tbody);
        }
        tbody.appendTo("#" + title+"_table");
    }

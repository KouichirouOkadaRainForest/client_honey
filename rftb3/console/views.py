import os
import glob
import json
import shutil
import jstree
import pprint
import datetime
import requests

from django.conf import settings
from django.shortcuts import render
from django.template import Context, Template
from django.http import JsonResponse
from django.http import FileResponse
from django.shortcuts import redirect
from django.contrib.auth import authenticate, login

gLogDir = os.getenv("ANALYSYS_HOME")
gConfDir = os.getenv("ANALYSYS_CONF")
gAnalysysLogDir = os.getenv("ANALYSYS_LOG")

def console_start(request):
    if not request.user.is_authenticated:
        return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

#    if not request.user.is_authenticated:
#        return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

    """Start page with a documentation.
    """
    return render(
        request,
        "django_sb_admin/start.html",
        {
            "nav_active": "start"
        }
    )

def console_login(request):
    """Start page with a documentation.
    """
    return render(
        request,
        "django_sb_admin/login.html"
    )

def console_login_auth(request):
    username = request.POST.get('inputID')
    password = request.POST.get('inputPassword')
    user = authenticate(request, username=username, password=password)
    if user is not None:
        login(request, user)
        # Redirect to a success page.
        return redirect('/console/') 
    else:
        return redirect('/console/login/') 
#    return render(
#        request,
#        "django_sb_admin/login.html"
#    )

def console_logout(request):
    logout(request)
    return redirect('/console/login/') 

def dashboard(request):
    """Dashboard page.
    """
    return render(
        request,
        "django_sb_admin/sb_admin_dashboard.html",
        {
            "nav_active": "dashboard"
        }
    )

def charts(request):
    """Charts page.
    """
    return render(request, "django_sb_admin/sb_admin_charts.html",
                  {"nav_active":"charts"})
def tables(request):
    """Tables page.
    """
    return render(request, "django_sb_admin/sb_admin_tables.html",
                  {"nav_active":"tables"})
def forms(request):
    """Forms page.
    """
    return render(request, "django_sb_admin/sb_admin_forms.html",
                  {"nav_active":"forms"})
def bootstrap_elements(request):
    """Bootstrap elements page.
    """
    return render(request, "django_sb_admin/sb_admin_bootstrap_elements.html",
                  {"nav_active":"bootstrap_elements"})
def bootstrap_grid(request):
    """Bootstrap grid page.
    """
    return render(request, "django_sb_admin/sb_admin_bootstrap_grid.html",
                  {"nav_active":"bootstrap_grid"})
def dropdown(request):
    """Dropdown  page.
    """
    return render(request, "django_sb_admin/sb_admin_dropdown.html",
                  {"nav_active":"dropdown"})
def rtl_dashboard(request):
    """RTL Dashboard page.
    """
    return render(request, "django_sb_admin/sb_admin_rtl_dashboard.html",
                  {"nav_active":"rtl_dashboard"})

def blank(request):
    """Blank page.
    """
    return render(request, "django_sb_admin/sb_admin_blank.html",
                  {"nav_active":"blank"})

def crawl_regist(request):
    if not request.user.is_authenticated:
        return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

    context = {}
    return render(
        request,
        "django_sb_admin/crawl_regist.html",
        context
    )

def crawl_history(request):
    if not request.user.is_authenticated:
        return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

    context = {}
    return render(
        request,
        "django_sb_admin/crawl_history.html",
        context
    )

def crawl_regist_done(request):
    if not request.user.is_authenticated:
        return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

    context = {}
    context["url"] = request.GET.get('url')
    context["id"] = request.GET.get('session_id')
    return render(
        request,
        "django_sb_admin/crawl_regist_done.html",
        context
    )

def result(request):
    if not request.user.is_authenticated:
        return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

    context = {}
    context["session_id"] = request.GET.get('session_id')
    return render(
        request,
        "django_sb_admin/result.html",
        context
    )

def result_info(request):
    if not request.user.is_authenticated:
        return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

    context = {}
    context["session_id"] = request.GET.get('session_id')
    return render(
        request,
        "django_sb_admin/result_info.html",
        context
    )

def result_info_link(request):
    if not request.user.is_authenticated:
        return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

    context = {}
    context["session_id"] = request.GET.get('session_id')
    return render(
        request,
        "django_sb_admin/result_info_link.html",
        context
    )

def result_snapshot(request):
    if not request.user.is_authenticated:
        return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

    context = {}
    context["session_id"] = request.GET.get('session_id')
    return render(
        request,
        "django_sb_admin/result_snapshot.html",
        context
    )

def result_domain(request):
    if not request.user.is_authenticated:
        return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

    context = {}
    context["session_id"] = request.GET.get('session_id')
    return render(
        request,
        "django_sb_admin/result_domain.html",
        context
    )

def result_country(request):
    if not request.user.is_authenticated:
        return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

    context = {}
    context["session_id"] = request.GET.get('session_id')
    return render(
        request,
        "django_sb_admin/result_country.html",
        context
    )

def result_content_type(request):
    if not request.user.is_authenticated:
        return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

    context = {}
    context["session_id"] = request.GET.get('session_id')
    return render(
        request,
        "django_sb_admin/result_content_type.html",
        context
    )

def result_har_info(request):
    if not request.user.is_authenticated:
        return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

    context = {}
    context["session_id"] = request.GET.get('session_id')
    return render(
        request,
        "django_sb_admin/result_har_info.html",
        context
    )

def result_har_summary(request):
    if not request.user.is_authenticated:
        return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

    context = {}
    context["session_id"] = request.GET.get('session_id')
    return render(
        request,
        "django_sb_admin/result_har_summary.html",
        context
    )

def result_har_hist(request):
    if not request.user.is_authenticated:
        return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

    context = {}
    context["session_id"] = request.GET.get('session_id')
    return render(
        request,
        "django_sb_admin/result_har_hist.html",
        context
    )

def result_har_attention(request):
    if not request.user.is_authenticated:
        return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

    context = {}
    context["session_id"] = request.GET.get('session_id')
    return render(
        request,
        "django_sb_admin/result_har_attention.html",
        context
    )

def result_sandbox_info(request):
    if not request.user.is_authenticated:
        return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

    context = {}
    context["session_id"] = request.GET.get('session_id')
    return render(
        request,
        "django_sb_admin/result_sandbox_info.html",
        context
    )


def codeview(request):
    context = {}
    session_id = request.GET.get("session_id").replace('"','')
    f_hash = request.GET.get("hash").replace('"','')
    data_path = os.path.join( os.environ.get('CRAWL_DATA', './tmp'), session_id, "sandbox", f_hash)
    f = open( data_path )
    context["code"] = f.read()
    f.close()

    return render(
        request,
        "django_sb_admin/codeview.html",
        context
    )

def download(request):
    context = {}
    session_id = request.GET.get("session_id").replace('"','')
    save_path = os.path.join( "/tmp", session_id )
    data_path = os.path.join( os.environ.get('CRAWL_DATA', './tmp'), session_id )

    shutil.make_archive(save_path, 'zip', root_dir=data_path)

    file_path = save_path+".zip"
    response = FileResponse(open(file_path, 'rb'))
    response['Content-Type'] = 'application/octet-stream'
    response['Content-Disposition'] = 'attachment;filename="{0}"'.format(os.path.basename(file_path))
    response['Content-Length'] = os.path.getsize(file_path)

    os.remove(file_path)

    return response

def alert_done(request):
    context = {}
#    context["datas"] = {}
#    for filename in glob.glob( os.path.join(gLogDir, "alert_result", "alert_*") ) :
#        title = "/".join(filename.split('_')[2:3])
#        f = open( filename )
#        tmp = []
#        for line in f :
#            tmp.append( json.loads(line) )
#        context["datas"][title] = json.dumps(tmp[0:2])
#        f.close()

    return render(
        request,
        "django_sb_admin/alert_done.html",
        context
    )

def alert_done_list(request):
    data = {}
    #for filename in glob.glob( os.path.join(gLogDir, "LOG", "alert_result", "alert_*.*") ) :
    #for filename in glob.glob( os.path.join(gLogDir, "alert_result", "alert_*.*") ) :
    for filename in glob.glob( os.path.join(gLogDir, "alert_result", "alert_json*.*") ) :
        file_base = os.path.basename(filename)
        #title = file_base
        title = "_".join(file_base.split('_')[2:4])
        data[title] = []
        f = open( filename )
        for line in f :
            data[title].append( json.loads(line) )
        f.close()

    return JsonResponse(data)

def alert_history(request):
    return render(
        request,
        "django_sb_admin/alert_history.html"
    )

def alert_history_list(request):
    data = {}
    #for filename in glob.glob( os.path.join(gLogDir, "LOG", "alert_result", "done*.*") ) :
    for filename in glob.glob( os.path.join(gLogDir, "alert_result", "done*.*") ) :
        file_base = os.path.basename(filename)
        #title = file_base
        title = "_".join(file_base.split('_')[3:5])
        data[title] = []
        f = open( filename )
        for line in f :
            data[title].append( json.loads(line) )
        f.close()

    return JsonResponse(data)

def alert_log(request):
    return render(
        request,
        "django_sb_admin/alert_log.html"
    )

def alert_log_tree(request):
    target_path = os.path.join(gLogDir, "alert_result")
    paths = []
    cnt = 1
    for (root, dirs, files) in os.walk(target_path):
        for file in files:
            path = os.path.join(root,file)
            #path = path.replace(target_path,"/")
            obj = jstree.Path(path, cnt)
            paths.append(obj)
            cnt += 1

    t = jstree.JSTree(paths)
    d = t.jsonData()
    return JsonResponse(d[0])

def scanner_alert(request):
    return render(
        request,
        "django_sb_admin/scanner_alert.html"
    )

def scanner_alert_list(request):
    data = {}
    #for filename in glob.glob( os.path.join(gLogDir, "LOG", "scanner", "alert*.*") ) :
    for filename in glob.glob( os.path.join(gLogDir, "scanner", "alert*.*") ) :
        file_base = os.path.basename(filename)
        #title = file_base
        title = "".join(file_base.split('.')[0])
        data[title] = []
        f = open( filename )
        for line in f :
            data[title].append( json.loads(line) )
        f.close()

    return JsonResponse(data)

def scanner_alert_log(request):
    return render(
        request,
        "django_sb_admin/scanner_alert_log.html"
    )

def scanner_alert_log_list(request):
    data = {}
    keys = ['item1', 'item2', 'item3', 'item4', 'item5', 'item6', 'item7', 'item8', 'item9']
    for filename in glob.glob( os.path.join(gLogDir, "scanner/alert", "*.json") ) :
    #for filename in glob.glob( os.path.join("../orig/", "LOG", "scanner/alert", "*.json" ) )  :
        print( filename )
        file_base = os.path.basename(filename)
        #title = file_base
        title = "".join(file_base.split('.')[0])
        data[title] = []
        f = open( filename )
        for line in f :
            tmp = line.split(',')
            dic = dict(zip(keys, tmp))
            data[title].append( dic )
        f.close()

    return JsonResponse(data)

def scanner_octet(request):
    return render(
        request,
        "django_sb_admin/scanner_octet.html"
    )

def scanner_octet_tree(request):
    target_path = os.path.join(gLogDir, "scanner")
    paths = []
    cnt = 1
    for (root, dirs, files) in os.walk(target_path):
        for file in files:
            path = os.path.join(root,file)
            if 'history' in path :
                #path = path.replace(target_path,"/")
                obj = jstree.Path(path, cnt)
                paths.append(obj)
                cnt += 1

    t = jstree.JSTree(paths)
    d = t.jsonData()
    return JsonResponse(d[0])

def scanner_sensor(request):
    return render(
        request,
        "django_sb_admin/scanner_sensor.html"
    )

def scanner_sensor_tree(request):
    target_path = os.path.join(gLogDir, "scanner")
    paths = []
    cnt = 1
    for (root, dirs, files) in os.walk(target_path):
        for file in files:
            path = os.path.join(root,file)
            if ('alert' not in path) and ('history' not in path) :
                #path = path.replace(target_path,"/")
                obj = jstree.Path(path, cnt)
                paths.append(obj)
                cnt += 1

    t = jstree.JSTree(paths)
    d = t.jsonData()
    return JsonResponse(d[0])

def other_logs(request):
    return render(
        request,
        "django_sb_admin/other_logs.html"
    )

def other_logs_tree(request):
    target_path = gAnalysysLogDir
    paths = []
    cnt = 1
    for (root, dirs, files) in os.walk(target_path):
        for file in files:
            path = os.path.join(root,file)
            #path = path.replace(target_path,"/")
            obj = jstree.Path(path, cnt)
            paths.append(obj)
            cnt += 1

    t = jstree.JSTree(paths)
    d = t.jsonData()
    return JsonResponse(d[0])

def editor(request):
    context = {}
    if "source" in request.GET:
        param_value = request.GET.get("source")
        context["source"] = param_value
        f = open( os.path.join(gConfDir,param_value) )
        context["code"] = f.read()
        f.close()
    else :
        context["code"] = ""

    return render(
        request,
        "django_sb_admin/editor.html",
        context
    )

def editor_save(request):
    context = {}

    source = request.POST.get('source', None)
    new_code = request.POST.get('change_code', None)

    target = os.path.join(gConfDir,source)
    now = datetime.datetime.now()
    backup = "%s.%s"%( target, now.strftime('%Y%m%d') )

    shutil.copy(target, backup)

    f = open( target, "w" )
    f.write( new_code )
    f.close()

    url = "../editor/?source=%s"%(source)
    return redirect(url)


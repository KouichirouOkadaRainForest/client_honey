from django.conf.urls import url
import console.views
from django.views.generic import TemplateView
from django.urls import path

from django.views.generic import RedirectView

urlpatterns = [
    url(r'^$', console.views.console_start, name='sb_admin_start'),

    url(r'^login/$', console.views.console_login, name='sb_admin_login'),
    url(r'^login/auth/$', console.views.console_login_auth, name='sb_admin_login_auth'),
    url(r'^logout/$', console.views.console_logout, name='sb_admin_logout'),

    url(r'^crawl_regist/$',       console.views.crawl_regist, name='crawl_regist'),
    url(r'^crawl_history/$',      console.views.crawl_history, name='crawl_history'),
    url(r'^crawl_regist_done/$',  console.views.crawl_regist_done, name='crawl_regist_done'),

    url(r'^result/$',            console.views.result_info, name='result'),
    url(r'^result/info/$',       console.views.result_info, name='result_info'),
    url(r'^result/info/link/$',    console.views.result_info_link, name='result_info_link'),
    url(r'^result/info/snapshot/$',    console.views.result_snapshot, name='result_snapshot'),
    url(r'^result/domain/$',       console.views.result_domain, name='result_domain'),
    url(r'^result/country/$',       console.views.result_country, name='result_country'),
    url(r'^result/content_type/$', console.views.result_content_type, name='result_content_type'),
    url(r'^result/har/info/$',     console.views.result_har_info, name='result_har_info'),
    url(r'^result/har/summary/$',     console.views.result_har_summary, name='result_har_summary'),
    url(r'^result/har/hist$',      console.views.result_har_hist, name='result_har_hist'),
    url(r'^result/har/attention$',      console.views.result_har_attention, name='result_har_attention'),
    url(r'^result/sandbox/info$',      console.views.result_sandbox_info, name='result_sanbox_info'),
    url(r'^result/code/$',      console.views.codeview, name='codeview'),
    url(r'^result/download/$',      console.views.download, name='download'),

    path('register/', TemplateView.as_view(
        template_name="console/register.html"),
        name='sb_admin_register'
    ),
    path('forgot_password/', TemplateView.as_view(
        template_name="console/forgot_password.html"),
        name='sb_admin_forgot_password'
    ),
    path('404/', TemplateView.as_view(
        template_name="console/sb_admin_404.html"),
        name='sb_admin_404'
    ),
    url(r'^dashboard/$', console.views.dashboard, name='sb_admin_dashboard'),
    url(r'^charts/$', console.views.charts, name='sb_admin_charts'),
    url(r'^tables/$', console.views.tables, name='sb_admin_tables'),
    url(r'^forms/$', console.views.forms, name='sb_admin_forms'),
    url(r'^bootstrap-elements/$', console.views.bootstrap_elements, name='sb_admin_bootstrap_elements'),
    url(r'^bootstrap-grid/$', console.views.bootstrap_grid, name='sb_admin_bootstrap_grid'),
    url(r'^rtl-dashboard/$', console.views.rtl_dashboard, name='sb_admin_rtl_dashboard'),
    url(r'^blank/$', console.views.blank, name='sb_admin_blank'),
]

